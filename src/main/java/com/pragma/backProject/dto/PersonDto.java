package com.pragma.backProject.dto;

public class PersonDto {
    private String name;
    private String phoneNumber;
    private String email;

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }
}
