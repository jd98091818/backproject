package com.pragma.backProject.dto;

import com.pragma.backProject.data.Person;

import java.util.Date;

public class ImageDto {
    private String name;
    private Date date;
    private String personEmail;

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public String getPersonEmail() {
        return personEmail;
    }
}
