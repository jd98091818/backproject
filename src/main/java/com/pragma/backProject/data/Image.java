package com.pragma.backProject.data;

import com.pragma.backProject.dto.ImageDto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="image")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private Date date;
    private String FileImage;

    @ManyToOne
    @JoinColumn(name = "id_person")
    private Person person;

    public Image(){

    }

    public Image(ImageDto imageDto,Person person){
        this.name=imageDto.getName();
        this.date=imageDto.getDate();
        this.person= person;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public String getFileImage() {
        return FileImage;
    }

    public Person getPerson() {
        return person;
    }
}
