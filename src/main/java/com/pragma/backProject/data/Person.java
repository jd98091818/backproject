package com.pragma.backProject.data;


import com.pragma.backProject.dto.PersonDto;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name="person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    private String phoneNumber;
    private String email;
    @OneToMany(mappedBy = "person")
    private Collection<Image> images;

    public Person(){}
    public Person(PersonDto personDto){
        this.name = personDto.getName();
        this.email = personDto.getEmail();
        this.phoneNumber = personDto.getPhoneNumber();
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Collection<Image> getImages() {
        return images;
    }
}
