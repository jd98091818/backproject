package com.pragma.backProject.controller;

import com.pragma.backProject.data.Image;
import com.pragma.backProject.data.Person;
import com.pragma.backProject.dto.ImageDto;
import com.pragma.backProject.dto.PersonDto;
import com.pragma.backProject.service.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/v1")
public class Controller {
    private final Services service;

    public Controller(@Autowired Services service){
        this.service = service;
    }
    @PostMapping("/person")
    public ResponseEntity<Person> createPerson(@RequestBody PersonDto person){
        service.createPerson(person);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/image")
    public ResponseEntity<Image> createImage(@RequestBody ImageDto image, @RequestParam("file")MultipartFile imageFile){

        service.createImage(image);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }



}
