package com.pragma.backProject.repository;

import com.pragma.backProject.data.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImagenRepository extends JpaRepository<Image,Integer> {
}
