package com.pragma.backProject.repository;

import com.pragma.backProject.data.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


public interface PersonaRepository extends JpaRepository<Person,Integer> {
    Optional<Person> findByEmail(String email);
}
