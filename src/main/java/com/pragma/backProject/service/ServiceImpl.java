package com.pragma.backProject.service;

import com.pragma.backProject.data.Image;
import com.pragma.backProject.data.Person;
import com.pragma.backProject.dto.ImageDto;
import com.pragma.backProject.dto.PersonDto;
import com.pragma.backProject.repository.ImagenRepository;
import com.pragma.backProject.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements Services{
    @Autowired
    private PersonaRepository personaRepo;
    @Autowired
    private ImagenRepository imagenRepository;

    @Override
    public void createPerson(PersonDto person){
        Person newPerson = new Person(person);
        personaRepo.save(newPerson);
    }

    @Override
    public void createImage(ImageDto imageDto) {
        Person person = personaRepo.findByEmail(imageDto.getPersonEmail()).orElse(null);
        if (person!=null){
            Image image = new Image(imageDto,person);
            imagenRepository.save(image);

        }
    }
}
