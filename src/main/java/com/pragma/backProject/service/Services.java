package com.pragma.backProject.service;

import com.pragma.backProject.data.Person;
import com.pragma.backProject.dto.ImageDto;
import com.pragma.backProject.dto.PersonDto;

public interface Services {
    public void createPerson(PersonDto person);
    public void createImage(ImageDto imageDto);

}
